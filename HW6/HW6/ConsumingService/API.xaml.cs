﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using QuickType;

using Newtonsoft.Json;

using System.ComponentModel;
using System.Diagnostics;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Net.Http.Headers;

namespace HW6.ConsumingService
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class API : INotifyPropertyChanged 
    {
        
         #region Properties

         private List<PostsItemModel> _postsList { get; set; }
         public List<PostsItemModel> PostsList
         {

             get 
             {
                 return _postsList;
             }

             set
             {
                 if (value != _postsList)
                 {
                     _postsList = value;
                     NotifyPropertyChanged();
                 }
             }
         }

         #endregion

         public event PropertyChangedEventHandler PropertyChanged;
         protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
         {
             PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
         }
         
        public API()
        {
            InitializeComponent();
            BindingContext = new PostsItemModel();
        }

        public async void Button_Clicked(object sender, EventArgs e)
        {
           
            //what we build and send to the server
            HttpClient client = new HttpClient();
            var uri = new Uri(
                string.Format(
                    $"https://owlbot.info/api/v4/dictionary/handsaw" //+ $"5485b52a4e68b42e14fa6cdad1c84a140ad64d5c"
                    ));
            var request = new HttpRequestMessage();
            request.Method = HttpMethod.Get;
            request.RequestUri = uri;


           
            request.Headers.Add("Application", "application / json");
            client.DefaultRequestHeaders.Authorization =
    new AuthenticationHeaderValue("Token", "5485b52a4e68b42e14fa6cdad1c84a140ad64d5c");//my api token
            
            
            HttpResponseMessage response = await client.SendAsync(request);
            PostsItemModel wordData = null;
            //await is used so that we dont execute the next lines of code in the function until after we get a response from the server
            //var posts = JsonConvert.DeserializeObject<List<Posts>>(response);
            //If the http status code is 200 it means OK(successful) in the response, then read the content in the response
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                //var pr = JsonConvert.DeserializeObject<List<PostsItemModel>>(content);
               // ObservableCollection<PostsItemModel> trends = new ObservableCollection<PostsItemModel>(pr);
               // myList.ItemsSource = trends;

                wordData = PostsItemModel.FromJson(content);
               // Definition chap = PostsItemModel.FromJson(content);
                //postData = PostsItemModel.FromJson(content);
                Debug.WriteLine("SUCCESSFUL!");
                Debug.WriteLine(content);
                Debug.WriteLine("PostsList:::::::::::");
                Debug.WriteLine(wordData.Word);
                Debug.WriteLine(wordData.Pronunciation);
               // Debug.WriteLine(PostsItemModel.Definition.Type);
            }
            else
            {
                Debug.WriteLine("An error occured while loading data");
            }
        }

    }


}
