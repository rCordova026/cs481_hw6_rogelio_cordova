﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Text;
using QuickType;

//Need to create a httpClient object to use

namespace HW6.ConsumingService
{
    public class APIViewModel //: INotifyPropertyChanged
    {
        /*
        #region Properties
       
        private List<PostsItemModel> _postsList { get; set; }
        public List<PostsItemModel> PostsList
        {

            get 
            {
                return _postsList;
            }

            set
            {
                if (value != _postsList)
                {
                    _postsList = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion
        
        public event PropertyChangedEventHandler PropertyChanged;
        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        */
        public APIViewModel()
        {
           // GetDataAsync();
             
        }

        async void GetDataAsync()
        {
            //var uri = new Uri(string.Format(Constants.TodoItemsUrl, string.Empty));
            //what we build and send to the server
            HttpClient httpClient = new HttpClient();
            var uri = new Uri(
                string.Format(
                    $"https://owlbot.info/api/v4/dictionary/owl" //+ $"5485b52a4e68b42e14fa6cdad1c84a140ad64d5c"
                    ));
            var request = new HttpRequestMessage();
            request.Method = HttpMethod.Get;
            request.RequestUri = uri;
            //httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization:", "Token 5485b52a4e68b42e14fa6cdad1c84a140ad64d5c");
            request.Headers.Add("5485b52a4e68b42e14fa6cdad1c84a140ad64d5c", "application/json");
            HttpResponseMessage response = await httpClient.SendAsync(request);
           // PostsItemModel Definition = null;
           ///// var response = await httpClient.GetAsync("https://owlbot.info/api/v4/dictionary/douched"); //await is used so that we dont execute the next lines of code in the function until after we get a response from the server
            //var posts = JsonConvert.DeserializeObject<List<Posts>>(response);
            //If the http status code is 200 it means OK(successful) in the response, then read the content in the response
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                //var posts = JsonConvert.DeserializeObject<List<PostsItemModel>>(content);
             //  var postsItemModel = PostsItemModel.FromJson(content);
               //var posts = Posts.FromJson(jsonString);
               // PostsList = new List<Posts>(posts);

            }
            else
            {
                Debug.WriteLine("An error occured while loading data");
            }
        }

    }

    /*
    public class Definition
    {
        public string type { get; set; }
        public string definition { get; set; }
        public string example { get; set; }
        public string image_url { get; set; }
        public object emoji { get; set; }
    }
    
   
   
    public class Posts
     {
         public List<Definition> definitions { get; set; }
         public string word { get; set; }
         public string pronunciation { get; set; }
     }
    */
}
