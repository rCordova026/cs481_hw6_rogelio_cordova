﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using HW6.ConsumingService;
namespace HW6
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new API());
                //MainPage();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
